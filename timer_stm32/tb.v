`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.12.2018 18:00:59
// Design Name: 
// Module Name: tb1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb1();

parameter W = 32;

reg clk, rst;
reg cyc_m2s, we_m2s;
reg [W-1:0] data_m2s;
reg [W-1:0] addr_m2s;

wire [W-1:0] data_s2m;
wire ack_s2m;
wire err_s2m;


always #5 clk = !clk;

timer #(.W(W))t1(.clk(clk), .rst(rst), .cyc_m2s(cyc_m2s), .we_m2s(we_m2s), 
    .addr_m2s(addr_m2s), .data_m2s(data_m2s), .data_s2m(data_s2m),
    .err_s2m(err_s2m), .ack_s2m(ack_s2m));
    
initial
begin
    clk <= 0;
    rst = 1;
    cyc_m2s = 0;
    we_m2s = 0;
    data_m2s = 0;
    addr_m2s = 0;
    
    repeat (2) @(posedge clk);
    
    @(posedge clk) rst = 0;
    
    //WRITE 
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    //TIM1_CR1_addr 
        data_m2s = 32'h00000081;    //enable de counter and the TIM1_ARR limit
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
	//Write in the TIM_ARR
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h4001002C;    //TIM_ARR_addr
        data_m2s = 32'h00000010;    //the number until we want to count
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);
    
	//Write in the prescaler
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010028;
        data_m2s = 32'h00000001;
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    //ERROR WRITE
    repeat (2) @(posedge clk);
    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010007;
        data_m2s = 32'h01010101;
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;       
    end

    repeat (2) @(posedge clk);
    
    //READ 
    @(posedge clk);
    begin
        cyc_m2s = 1;
        addr_m2s = 32'h40010028;        
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        
        
    end
    
    repeat (2) @(posedge clk);   

    @(posedge clk);
    begin
        cyc_m2s = 1;
        addr_m2s = 32'h40010000;        
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        
        
    end
   
    //ERROR READ
    repeat (2) @(posedge clk);   
    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        addr_m2s = 32'h40010001;        
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        
        
    end
    
    repeat (2) @(posedge clk);
        
    //WRITE again prescaler
    repeat (2) @(posedge clk);
        
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010028;
        data_m2s = 32'h00000000;
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    #100 $finish;    
end
endmodule

