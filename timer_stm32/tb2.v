`timescale 1ns / 100ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.05.2019 13:20:50
// Design Name: 
// Module Name: tb2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb2();

parameter W = 32;

reg clk, rst;
reg cyc_m2s, we_m2s;
reg [W-1:0] data_m2s;
reg [W-1:0] addr_m2s;

wire [W-1:0] data_s2m;
wire ack_s2m;
wire err_s2m;


always #5 clk = !clk;

timer #(.W(W))t1(.clk(clk), .rst(rst), .cyc_m2s(cyc_m2s), .we_m2s(we_m2s), 
    .addr_m2s(addr_m2s), .data_m2s(data_m2s), .data_s2m(data_s2m),
    .err_s2m(err_s2m), .ack_s2m(ack_s2m));
    
initial
begin
    clk <= 0;
    rst = 1;
    cyc_m2s = 0;
    we_m2s = 0;
    data_m2s = 0;
    addr_m2s = 0;
    
    repeat (2) @(posedge clk);
    
    @(posedge clk) rst = 0;
    
    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h000000E1;    //CEN = 1, ARPE = 1 (SR updated at uev) and CMS = 11 (center-aligned mode)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_ARR    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h4001002C;    
        data_m2s = 32'h00000006;    //the number until we want to count
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_PSC
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010028;
        data_m2s = 32'h00000001;    //clk frequency divided by 2(1+1)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    repeat (2) @(posedge clk);

    //TIM1_RCR
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010030;
        data_m2s = 32'h00000001;    //Number of over/underflows in order to generate an UEV
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
   
    repeat (2) @(posedge clk);
    
    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000081;    //CEN = 1, ARPE = 1 and CMS = 00 (edge-aligned mode) DIR = 0 (upcounting)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_PSC
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010028;
        data_m2s = 32'h00000000;    //clk frequency divided by 1 (0+1)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);
    
    
    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000011;    //CEN = 1, ARPE = 0 (ARR updated instantly) and CMS = 00 DIR = 1 (downcounting)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_ARR    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h4001002C;    
        data_m2s = 32'h00000004;    //the number until we want to count
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end    
    
    repeat (2) @(posedge clk);
    
    
    //TIM1_RCR
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010030;
        data_m2s = 32'h00000000;    //Number of over/underflows in order to generate an UEV
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end

    repeat (2) @(posedge clk);
    
    //TIM1_DIER (masks)
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h4001000C;
        data_m2s = 32'h00000001;        //UIE enabled (update interrupt flag set on next UEV)
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);

    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000009;    //CEN = 1, ARPE = 0 and CMS = 00 DIR = 0 (upcounting) UDIS = 0 OPM = 1
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (10) @(posedge clk);
    
    //Reinitializes the counter
    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000013;    //CEN = 1, ARPE = 0 and CMS = 00 DIR = 1 UDIS = 1 (no update events occur) OPM = 0
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_ARR    
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h4001002C;    
        data_m2s = 32'h00000003;    //the number until we want to count
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end    
    
    repeat (2) @(posedge clk);
    

    //TIM1_PSC
    @(posedge clk);
    begin
       cyc_m2s = 1;
       we_m2s = 1;
       addr_m2s = 32'h40010028;
       data_m2s = 32'h00000001;    //clk frequency divided by 2 (1+1)
    end
    
    repeat (2) @(posedge clk);
    begin
       cyc_m2s = 0;
       we_m2s = 0;
       
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000063;    //CEN = 1, ARPE = 0 and CMS = 11 UDIS = 0
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    
    repeat (2) @(posedge clk);


    //TIM1_SR (flags)
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010010;
        data_m2s = 32'h00000000;        //UIF = 0
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);

    //TIM1_PSC
    @(posedge clk);
    begin
       cyc_m2s = 1;
       we_m2s = 1;
       addr_m2s = 32'h40010028;
       data_m2s = 32'h00000000;    //clk frequency divided by 1 (0+1)
    end
    
    repeat (2) @(posedge clk);
    begin
       cyc_m2s = 0;
       we_m2s = 0;
       
    end
    
    repeat (2) @(posedge clk);



    //TIM1_CR1
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010000;    
        data_m2s = 32'h00000061;    //CEN = 1, ARPE = 0 CMS = 11 UDIS = 0
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;     
    end
    
    repeat (2) @(posedge clk);
    
    //TIM1_EGR (Event generator)
    @(posedge clk);
    begin
        cyc_m2s = 1;
        we_m2s = 1;
        addr_m2s = 32'h40010014;
        data_m2s = 32'h00000001;        //UG = 1
    end
    
    repeat (2) @(posedge clk);
    begin
        cyc_m2s = 0;
        we_m2s = 0;
        
    end
    
    repeat (2) @(posedge clk);
    
    
    
    #100 $finish;
   
end  
endmodule